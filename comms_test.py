#!/usr/bin/python3 

import pprint
import click
from flask import Flask, request, make_response, jsonify
import flask  # - unfortunately need this to remove development warning banner
import json
import logging
import random
import socket
import sys
import requests


@click.group(help='Command line tool to help test communications with remote clients/hosts')
def cli():
    pass

@click.command(help='Listen for incoming messages from a remote client')
@click.option('-p', '--port', help='Port to listen on', required=True, type=int)
@click.option('-i', '--ip', help='IP address to listen on', default='localhost', type=str)
@click.option('-r', '--protocol_type', help='Protocol to listen for', default='binary', type=click.Choice(('binary', 'http')))
@click.option('-t', '--transport_type', help='Transport type', default='tcp', type=click.Choice(('tcp', 'udp')))
def listen(**kwargs):

    print('Setting up {transport_type} {protocol_type} listener at {ip}:{port}...'.format(transport_type=kwargs['transport_type'].upper(), protocol_type=kwargs['protocol_type'].upper(), ip=kwargs['ip'], port=kwargs['port']))

    if kwargs['transport_type'] == 'tcp':
        http_server = Flask(__name__)
        log = logging.getLogger('werkzeug')
        log.setLevel(logging.ERROR)
        flask.cli.show_server_banner = lambda *_: None

        @http_server.route("/", methods=['POST'])
        def generate_response():
            response_text =  "Successful HTTP connection. Hello {remoteaddr}!".format(remoteaddr=request.remote_addr)
            print(' * Received message from {remoteaddr}: "{message}"'.format(remoteaddr=request.remote_addr, message=request.get_json().get('message', 'Error: No data received from client')))
            print('   - Sending response: {}'.format(response_text))
            return jsonify({'message': response_text})

        http_server.run(host=kwargs['ip'], port=kwargs['port'], threaded=False, debug=False)



    if kwargs['protocol_type'] == 'binary':
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((kwargs['ip'], kwargs['port']))
        sock.listen(1)
        while True:
            print('Waiting for incoming data...\n')
            connection, client_address = sock.accept()

    elif kwargs['transport_type'] == 'udp':

        if kwargs['protocol_type'] == 'http':
            print('Error: Invalid combination. HTTP protocol over UDP transport is not supported.')
            sys.exit()

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((kwargs['ip'], kwargs['port']))
        while True:
            print('Waiting for incoming data...\n')
            message, address = sock.recvfrom(1024)



@click.option('-p', '--port', help='Port to send to', required=True, type=int)
@click.option('-i', '--ip', help='IP address to send to', default='localhost', type=str)
@click.option('-r', '--protocol_type', help='Protocol to send message with', default='binary', type=click.Choice(('binary', 'http')))
@click.option('-t', '--transport_type', help='Transport type', default='tcp', type=click.Choice(('tcp', 'udp')))
@click.option('-m', '--message', help='Message to send. If this is a filename, it will send the contents of the file.', default='Hello world! This is the "comms_test" program, reporting in.', type=str)
@click.command(help='Send test messages to a remote client')
def send(**kwargs):

    print('Sending message "{message}" via {transport_type} {protocol_type} to {ip}:{port}...'.format(message=kwargs['message'], transport_type=kwargs['transport_type'].upper(), protocol_type=kwargs['protocol_type'].upper(), ip=kwargs['ip'], port=kwargs['port']))

    if kwargs['transport_type'] == 'tcp':
        if kwargs['protocol_type'] == 'http':
            send_http_message(kwargs['ip'], kwargs['port'], kwargs['message'])
    

def send_http_message(ip, port, message):
    try:
        response = requests.post('http://{}:{}'.format(ip, port), json={'message': message})
        print(' * Server response: "{response}"'.format(response=response.json().get('message', 'Error: No data returned from server')))
    except requests.exceptions.ConnectionError as e:
        print(' * ERROR: Could not make connection to server ({ip}:{port})'.format(ip=ip, port=port))

if __name__ == '__main__':
    cli.add_command(listen)
    cli.add_command(send)
    cli()
